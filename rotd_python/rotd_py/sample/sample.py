from rotd_py.system import MolType
import rotd_py.rotdmath as rotd_math
import numpy as np
from abc import ABCMeta, abstractmethod
from ase.atoms import Atoms


class Sample(object):
    """Class used for generating random configuration of given fragments and
    dividing surface.

    Parameters
    ----------
    fragments : List of Fragment
        The franems in the system
    dividing_surface : Surface
        Current calculated surface considering all factets
    atom_distance_thred : float
        Too close atoms distance threshold.
    calculator: Calculator (ASE)
        Ase object calculaor for calculating the potential
    Attributes
    ----------
    div_surf : Surface
    close_dist : float
    calc : Calculator
    configuation : Atoms objects whici will be used to store the combination of
                   two fragments (the sampled configuraion) in Angstrom.
    dof_num : degree of freedom of the reaction system.
    weight : the geometry weight for the generated configuration.
    inf_energy: the energy of the configuration at infinite seperation in Hartree
    """

    def __init__(self, fragments=None, dividing_surface=None, calculator=None,
                 distance_thred=1.5, inf_energy=0.0, energy_size=1):
        __metaclass__ = ABCMeta
        # list of fragments

        self.fragments = fragments
        if fragments is None or len(fragments) < 2:
            raise ValueError("For sample class, \
                            at least two None framents are needed")
        self.div_surf = dividing_surface
        self.close_dist = distance_thred
        self._calc = calculator
        self.weight = 0.0
        self.inf_energy = inf_energy
        self.energy_size = energy_size
        self.energies = np.array([0.] * self.energy_size)
        self.ini_configuation()
        self.set_dof()

    def div_surf():
        return self.div_surf

    def set_div_surf(self, surface):
        self.div_surf = surface

    def set_dof(self):
        """Set up the degree of freedom of the system for calculation

        """
        self.dof_num = 3
        for frag in self.fragments:
            if frag.molecule_type == MolType.MONOATOMIC:
                continue
            elif frag.molecule_type == MolType.LINEAR:
                self.dof_num += 2
            elif frag.molecule_type == MolType.NONLINEAR:
                self.dof_num += 3

    def ini_configuation(self):
        """Initialize the total system with ASE class Atoms

        """
        new_atoms = []
        new_positions = []
        for frag in self.fragments:
            new_atoms += frag.get_chemical_symbols()
            for pos in frag.get_positions():
                new_positions.append(pos)
        self.configuration = Atoms(new_atoms, new_positions)
        self.configuration.set_calculator(self._calc)

    def get_dof(self):
        """return degree of freedom

        """
        return self.dof_num

    def get_canonical_factor(self):
        """return cannonical factor used in the flux calculation

        """
        return 2.0 * np.sqrt(2.0 * np.pi)

    def get_microcanonical_factor(self):
        """return microcannonical factor used in the flux calculation

        """
        return rotd_math.M_2_SQRTPI * rotd_math.M_SQRT1_2 / 2.0 / \
            rotd_math.gamma_2(self.get_dof() + 1)

    def get_ej_factor(self):
        """return e-j resolved ensemble factor used in the flux calculation,

        """
        return rotd_math.M_1_PI / rotd_math.gamma_2(self.get_dof() - 2)

    def get_tot_inertia_moments(self):
        """Return the inertia moments for the sampled configuraion

        """

        return self.configuration.get_moments_of_inertia() * \
            rotd_math.mp / rotd_math.Bohr**2

    def get_weight(self):
        return self.weight

    def are_atoms_close(self):
        """Check the distance among atoms between the two fragments.

        Returns
        -------
        type
            Boolean

        """
        lb_pos_0 = self.fragments[0].get_labframe_positions()
        lb_pos_1 = self.fragments[1].get_labframe_positions()
        for i in range(0, len(lb_pos_0)):
            for j in range(0, len(lb_pos_1)):
                dist = np.linalg.norm(lb_pos_0[i] - lb_pos_1[j])
                if dist < self.close_dist:
                    return True
        return False

    @abstractmethod
    def generate_configuration(self):
        """This funtion is an abstract method for generating random
        configuration based relative to different sample schema.
        1. generate the new configuation.
        2. set the self.weight
        3. set the new postions for self.configuration.

        """
        pass

    def get_energies(self):

        # return absolute energy in eV
        e = self.configuration.get_potential_energy()

        # TODO: need to fill the energy correction part

        self.energies[0] = e / rotd_math.Hartree - self.inf_energy
        return self.energies


# The following code can be helpful if one has already had some sampled configuration
# and want to do some tests. Otherwise, do not need to use the following code.
class geometry(object):
    def __init__(self, atoms=None, energy=0.0, weight=0.0, pivot_rot=None,
                 frag1_rot=None, frag2_rot=None, tot_im=None):
        self.atoms = atoms
        self.energy = np.array(energy)
        self.weight = weight
        self.tot_im = np.array(tot_im)
        self.pivot_rot = np.array(pivot_rot)
        self.frag1_rot = np.array(frag1_rot)
        self.frag2_rot = np.array(frag2_rot)

    def get_atoms(self):
        return self.atoms

    def get_energy(self):
        return self.energy

    def get_weight(self):
        return self.weight

    def get_pivot_rot(self):
        return self.pivot_rot

    def get_frag1_rot(self):
        return self.frag1_rot

    def get_frag2_rot(self):
        return self.frag2_rot

    def get_tot_im(self):
        return self.tot_im


def preprocess(file_name):
    file_lines = open(file_name).readlines()
    geometries = []
    atom_num = 8
    for i, line in enumerate(file_lines):

        if line.startswith('Geometry:'):
            line_index = i
            positions = None
            if len(line.split()) == 1:
                positions = np.zeros((atom_num, 3))
                for j in range(0, atom_num):
                    positions[j] = map(float, file_lines[i+1+j].split()[1:])
                line_index += atom_num

            energy = float(file_lines[line_index + 1].split()[1])
            weight = float(file_lines[line_index + 2].split()[1])
            tot_im = np.array(map(float, file_lines[line_index + 4].split()[:]))
            pivot_rot = map(float, file_lines[line_index + 6].split()[:])
            frag1_rot = map(float, file_lines[line_index + 8].split())
            frag2_rot = map(float, file_lines[line_index + 10].split())
            atoms = None
            if positions is not None:
                atoms = Atoms('CH3CH3', positions=positions)

            geom = geometry(atoms=atoms, energy=energy, weight=weight,
                            tot_im=tot_im,
                            frag1_rot=frag1_rot,
                            frag2_rot=frag2_rot, pivot_rot=pivot_rot)
            geometries.append(geom)

    return geometries
