from rotd_py.fragment.nonlinear import Nonlinear
from rotd_py.system import Surface
from rotd_py.multi import Multi
from rotd_py.sample.multisample import MultiSample
from rotd_py.flux.fluxbase import FluxBase

import numpy as np
from amp import Amp

def generate_grid(start, interval, factor, num_point):
    #return the grid needed for simulation
    #interval += interval * factor
    #start += interval
    #return an numpy array with length of num_point
    i = 1
    grid = [start]
    for i in range(1, num_point):
        start += interval
        grid.append(start)
        interval = interval*factor
    return np.array(grid)

# temperature, energy grid and angular momentum grid
temperature = generate_grid(10, 10, 1.05, 51)
energy = generate_grid(0, 10, 1.05, 169)
angular_mom = generate_grid(0, 1, 1.1, 40)

print temperature
#fragment info
ch3_1 = Nonlinear('CH3', positions=[[-0.0, .0, 0.0],
                                 [1.0788619988,0.0000000000,0.0000000000],
                                [-0.5394309994,-0.9343218982,0.0000000000],
                                [-0.5394309994,0.9343218982,0.0000000000]])

ch3_2 = Nonlinear('CH3', positions=[[-0.0, .0, 0.0],
                                 [1.0788619988,0.0000000000,0.0000000000],
                                [-0.5394309994,-0.9343218982,0.0000000000],
                                [-0.5394309994,0.9343218982,0.0000000000]])

#setting the dividing surfaces
divid_surf = [Surface({'0':np.array([[0.0,0.0,0.0]]),
                       '1':np.array([[0.0,0.0,0.0]])},
                        distances=np.array([[15.0]])),
              Surface({'0':np.array([[0.0,0.0,0.0]]),
                       '1':np.array([[0.0,0.0,0.0]])},
                        distances=np.array([[13.5]])),
              Surface({'0':np.array([[0.0,0.0,0.0]]),
                       '1':np.array([[0.0,0.0,0.0]])},
                        distances=np.array([[12.0]])),
              Surface({'0':np.array([[0.0,0.0,0.0]]),
                       '1':np.array([[0.0,0.0,0.0]])},
                        distances=np.array([[11.0]])),
              Surface({'0':np.array([[0.0,0.0,0.0]]),
                       '1':np.array([[0.0,0.0,0.0]])},
                        distances=np.array([[10.0]])),
              Surface({'0':np.array([[0.0,0.0,0.0]]),
                       '1':np.array([[0.0,0.0,0.0]])},
                        distances=np.array([[9.0]])),
              Surface({'0':np.array([[0.0,0.0,0.0]]),
                       '1':np.array([[0.0,0.0,0.0]])},
                        distances=np.array([[8.0]])),
                Surface({'0':np.array([[0.0,0.0,0.0]]),
                       '1':np.array([[0.0,0.0,0.0]])},
                        distances=np.array([[7.5]]))]

#divid_surf = [Surface({'0':np.array([[0.0,0.0,1.0],
#                                    [0.0, 0.0, -1.0]]),
#                       '1':np.array([[0.0,0.0,0.5]])},
#                        distances=np.array([[13.0],[ 12.5]]))]

#how to sample the two fragments
calc = Amp.load('ch3ch3_test.amp')
r_inf= -79.47971696 #RS2/cc-pvtz
ch3_sample = MultiSample(fragments=[ch3_1, ch3_2], calculator=calc,
                inf_energy=r_inf, energy_size=1,distance_thred=1.5)

#the flux info
flux_parameter={'pot_smp_max':2000, 'pot_smp_min': 100,
            'tot_smp_max' : 10000, 'tot_smp_min' :100,
            'flux_rel_err' : 5.0, 'smp_len' : 1}

flux_base = FluxBase(temp_grid = temperature,
                 energy_grid = energy,
                 angular_grid = angular_mom,
                flux_type = 'MICROCANNONICAL',
                flux_parameter=flux_parameter)

#start the final run
multi = Multi(sample=ch3_sample, dividing_surfaces=divid_surf, fluxbase=flux_base)
multi.run()
#multi.total_flux['0'].flux_array[0].run(50)
#multi.total_flux['0'].save_file(0)
print multi.total_flux['0'].flux_array[0].acct_smp()
print multi.total_flux['0'].flux_array[0].fail_smp()
print multi.total_flux['0'].flux_array[0].face_smp()
print multi.total_flux['0'].flux_array[0].close_smp()

print multi.total_flux['1'].flux_array[0].acct_smp()
print multi.total_flux['1'].flux_array[0].fail_smp()
print multi.total_flux['1'].flux_array[0].face_smp()
print multi.total_flux['1'].flux_array[0].close_smp()

